matplotlib==3.0.2
scipy==1.2.1
scikit-image==0.14.1
scikit-learn==0.20.1
opencv-contrib-python==3.4.2.16
tensorflow==1.14.0
Keras==2.2.4
